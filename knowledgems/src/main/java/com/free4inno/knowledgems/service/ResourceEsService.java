package com.free4inno.knowledgems.service;

import com.alibaba.fastjson.JSONObject;
import com.free4inno.knowledgems.dao.AttachmentDao;
import com.free4inno.knowledgems.dao.CommentDao;
import com.free4inno.knowledgems.dao.ResourceDao;
import com.free4inno.knowledgems.dao.ResourceESDao;
import com.free4inno.knowledgems.domain.Attachment;
import com.free4inno.knowledgems.domain.Comment;
import com.free4inno.knowledgems.domain.Resource;
import com.free4inno.knowledgems.domain.ResourceES;
import com.free4inno.knowledgems.utils.HighlightResultMapper;
import com.free4inno.knowledgems.utils.TikaUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.*;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Author HUYUZHU/LIYUNZE/LIUXINYUAN.
 * Date 2021/3/27 13:42.
 */

@Slf4j
@Service
@EnableScheduling
public class ResourceEsService {

    //ES搜索语句builder
    BoolQueryBuilder reBuilder = new BoolQueryBuilder();

    //ES资源更新线程池
    ExecutorService executorService = Executors.newCachedThreadPool();

    @Autowired
    private ResourceESDao resourceESDao;

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private AttachmentDao attachmentDao;

    @Autowired
    private ResourceDao resourceDao;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Value("${spring.data.elasticsearch.cluster-nodes}")
    private String esNodes;

    @Value("${attatchment.download.url}")
    private String downloadUrl;

    @Value("${es.index.resource.name}")
    private String indexName;

    // 复制索引
    public Boolean cloneIndex(String sourceIndex, String destIndex) {
        log.info(this.getClass().getName() + "----in----" + "复制索引" + "----");
        /* 使用 okhttp3 直接通过 http 操作 es */
        // set host
        String esHost = esNodes.substring(0, esNodes.indexOf(":"));
        String url = "http://" + esHost + ":9200/_reindex";
        // set body json
        Map<String, Object> jsonObject = new HashMap<>();
        jsonObject.put("source", new HashMap<String, Object>() {{
            put("index", sourceIndex);
        }});
        jsonObject.put("dest", new HashMap<String, Object>() {{
            put("index", destIndex);
        }});
        JSONObject json = new JSONObject(jsonObject);
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        // http request

        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder().url(url).post(body).build();
        OkHttpClient client = new OkHttpClient();
        try {
            Response response = client.newCall(request).execute();
            if (response.code() == 200) {
                log.info(this.getClass().getName() + "----out----" + "复制成功" + "----");
                response.close();
                return true;
            } else {
                log.info(this.getClass().getName() + "----out----" + "复制失败" + "----");
                response.close();
                return false;
            }

        } catch (IOException e) {
            e.printStackTrace();
            log.info(this.getClass().getName() + "----out----" + "复制异常" + "----");
            return false;
        }
    }

    // 重建索引
    public boolean rebuildIndex(String name) {
        log.info(this.getClass().getName() + "----in----" + "重建索引" + "----");
        try {
            // 1. delete all index
            resourceESDao.deleteAll();
            // 2. get all resource in database
            List<Resource> resourcesList = resourceDao.findAll();
            // 3. add every resource into index
            for (Resource resource : resourcesList) {
                ResourceES resourceES = new ResourceES();
                resourceESDao.save(writeResourceToESHTTP(resource, resourceES));
            }
            log.info(this.getClass().getName() + "----out----" + "重建索引成功" + "----");
            return true;
        } catch (Exception e) {
            log.info(this.getClass().getName() + "----out----" + "重建索引失败" + "----");
            return false;
        }
    }

    // 删除索引
    public boolean deleteIndex(String name) {
        log.info(this.getClass().getName() + "----in----" + "删除索引" + "----");
        try {
            elasticsearchTemplate.deleteIndex(name);
            log.info(this.getClass().getName() + "----out----" + "删除索引成功" + "----");
            return true;
        } catch (Exception e) {
            log.info(this.getClass().getName() + "----out----" + "删除索引失败" + "----");
            return false;
        }
    }

    // 在ES中更新新建的资源
    public void addResourceES(int id) {
        log.info(this.getClass().getName() + "----in----" + "在ES中更新新建的资源" + "----");
        Resource resource = resourceDao.findResourceById(id);
        ResourceES resourceES = new ResourceES();
        //使用线程池完成资源更新，避免用户等待时间过长
        executorService.execute(() -> writeResourceToESHTTP(resource, resourceES));
        log.info(this.getClass().getName() + "----out----" + "在ES中更新新建资源完毕" + "----");
    }

    // 在ES中更新编辑过的资源
    public void updateResourceES(int id) {
        log.info(this.getClass().getName() + "----in----" + "在ES中更新编辑过的资源" + "----");
        Resource resource = resourceDao.findResourceById(id);
        ResourceES resourceES = new ResourceES();
        //使用线程池完成资源更新，避免用户等待时间过长
        executorService.execute(() -> writeResourceToESHTTP(resource, resourceES));
        //resourceESDao.save(writeResourceToES(resource, resourceES));
        log.info(this.getClass().getName() + "----out----" + "在ES中更新编辑资源完毕" + "----");
    }

    public void updateResourceES() {
        log.info(this.getClass().getName() + "----in----" + "更新ES资源" + "----");
        // 获取更新周期，为了后续进行资源类型判断判断
        int period = Integer.parseInt(getPeriod());
        // log.info("周期为" + period + "秒");
        List<Resource> resourcesList = resourceDao.findAll();
        // 获取现在的时间
        LocalDateTime nowTime = LocalDateTime.now();
        // 获取上次更新时间（现在的时间减去更新周期）
        LocalDateTime lastUpdate = nowTime.minusSeconds(period);
        // 获取数据库资源中的编辑时间和新建时间
        for (Resource resource : resourcesList) {
            LocalDateTime createTime = resource.getCreateTime().toLocalDateTime();
            LocalDateTime editTime = resource.getEditTime().toLocalDateTime();
            // 判断条件为：编辑时间在上次更新之后，创建时间在上次更新之前，即为新编辑资源
            int id = resource.getId();
            if (createTime.isBefore(lastUpdate) && editTime.isAfter(lastUpdate)) {
                log.info("编辑时间在上次更新之后，创建时间在上次更新之前，即为新编辑资源" + "----");
                updateResourceES(id);
            }
            // 判断条件为：创建时间在上次更新之后,即为新增资源
            if (createTime.isAfter(lastUpdate)) {
                log.info("创建时间在上次更新之后,即为新增资源" + "----");
                addResourceES(id);
            }
        }
        log.info(this.getClass().getName() + "----out----" + "更新ES资源完毕" + "----");
    }

    // 在ES中删除资源
    public void deleteResourceES(int id) {
        log.info(this.getClass().getName() + "----in----" + "在ES中删除资源" + "----");
        try {
            ResourceES resourceES = new ResourceES();
            resourceES.setId(id);
            resourceESDao.delete(resourceES);
            //deleteAttachById(id); //因将父子文档改为嵌套文档，因此不再需要删除附件索引
            log.info(this.getClass().getName() + "----out----" + "在ES中删除资源完毕" + "----");
        } catch (Exception e) {
            e.printStackTrace();
            log.error(this.getClass().getName() + "----" + "在ES中删除资源" + "----failure----" + "删除失败" + "----");
        }
    }

    // ES搜索
    public Page<ResourceES> searchResourceES(int page, int size, String query, ArrayList<String> groupIds, ArrayList<String> labelIds, int searchTimes, boolean login) {
        log.info(this.getClass().getName() + "----in----" + "ES搜索" + "----");
        Page<ResourceES> resourcePage = new PageImpl<>(new ArrayList<>());
        if (searchTimes == 1) {
            reBuilder = new BoolQueryBuilder();
        }

        if ((query == null || query.isEmpty()) && (groupIds == null || groupIds.isEmpty()) && (labelIds == null || labelIds.isEmpty())) {
            Sort sort = Sort.by(Sort.Direction.DESC, "create_time");
            Pageable pageable = PageRequest.of(page, size, sort);
            if (login) {
                log.info("用户已登录，查询全部资源" + "----");
                resourcePage = resourceESDao.findAll(pageable);
            } else {
                log.info("用户未登录，查询公开资源" + "----");
                resourcePage = resourceESDao.findResourceESByPermissionId("1", pageable); //未登录只能查询公开资源
            }
        } else {
            Pageable pageable = PageRequest.of(page, size);
            if ((query != null) && (!query.isEmpty())) {
                HighlightBuilder highlightBuilder1 = new HighlightBuilder().field("attachment.attach_text");
                HighlightBuilder highlightBuilder2 = new HighlightBuilder().field("comment.comment");
                InnerHitBuilder innerHitBuilder1 = new InnerHitBuilder().setHighlightBuilder(highlightBuilder1);
                InnerHitBuilder innerHitBuilder2 = new InnerHitBuilder().setHighlightBuilder(highlightBuilder2);
                NestedQueryBuilder nestedQuery = new NestedQueryBuilder("attachment", new MatchQueryBuilder("attachment.attach_text", query), ScoreMode.Total).boost(0.2f).innerHit(innerHitBuilder1);
                NestedQueryBuilder nestedQuery2 = new NestedQueryBuilder("comment", new MatchQueryBuilder("comment.comment", query), ScoreMode.Total).boost(1.2f).innerHit(innerHitBuilder2);
                MatchQueryBuilder matchQueryBuilder1 = new MatchQueryBuilder("title", query).boost(1.4f);
                MatchQueryBuilder matchQueryBuilder2 = new MatchQueryBuilder("text", query).boost(1.2f);
                reBuilder = reBuilder.should(matchQueryBuilder1).should(matchQueryBuilder2).should(nestedQuery).should(nestedQuery2);

            }
            /*
             * BUG
             * 此处存在一个奇怪的bug，只单独搜索group_id=89即周报时，将无法搜索到任何内容！但搭配其他搜索条件时搜索正常，且单独搜索其他群组或标签搜索正常，检查索引一切正常。
             * 尝试了多种方案调查均无果，推测可能由于版本问题导致。
             * 只能暂时用这种方式，当只有一个搜索条件时重复添加几次，不让单独的一个标签或群组作为搜索条件，即可规避该bug。
             */
            if ((groupIds != null) && (!groupIds.isEmpty())) {
                for (String groupId : groupIds) {
                    reBuilder = reBuilder.must(termQuery("group_id", groupId));
                }
                if (groupIds.size() == 1) {
                    reBuilder = reBuilder.must(termQuery("group_id", groupIds.get(0)));
                    reBuilder = reBuilder.must(termQuery("group_id", groupIds.get(0)));
                }
            }
            if ((labelIds != null) && (!labelIds.isEmpty())) {
                for (String labelId : labelIds) {
                    reBuilder = reBuilder.must(termQuery("label_id", labelId));
                }
                if (labelIds.size() == 1) {
                    reBuilder = reBuilder.must(termQuery("label_id", labelIds.get(0)));
                    reBuilder = reBuilder.must(termQuery("label_id", labelIds.get(0)));
                }
            }
            // 按照命中率倒序
            ScoreSortBuilder scoreSortBuilder = new ScoreSortBuilder();
            // 按照时间倒序
            FieldSortBuilder timeSortBuilder = new FieldSortBuilder("create_time");
            timeSortBuilder.order(SortOrder.DESC);
            //高亮搜索设置
            HighlightBuilder.Field highlightBuilderField1 = new HighlightBuilder.Field("title");
            HighlightBuilder.Field highlightBuilderField2 = new HighlightBuilder.Field("text");
            // 构造SearchQuery
            SearchQuery searchQuery;
            if (login) {
                searchQuery = new NativeSearchQueryBuilder()
                        .withQuery(reBuilder)
                        .withPageable(pageable)
                        .withSort(scoreSortBuilder)
                        .withSort(timeSortBuilder)
                        .withMinScore(0.2f)
                        .withHighlightFields(highlightBuilderField1,highlightBuilderField2)
                        .build();
            } else {
                searchQuery = new NativeSearchQueryBuilder()
                        .withQuery(reBuilder)
                        .withFilter(termsQuery("permissionId", "1"))
                        .withPageable(pageable)
                        .withSort(scoreSortBuilder)
                        .withSort(timeSortBuilder)
                        .withMinScore(0.2f)
                        .withHighlightFields(highlightBuilderField1,highlightBuilderField2)
                        .build();

            }//用户未登录，只能查询公开信息
            resourcePage = elasticsearchTemplate.queryForPage(searchQuery, ResourceES.class,new HighlightResultMapper());
        }
        log.info(this.getClass().getName() + "----out----" + "返回ES查询到的resourcePage" + "----");
        return resourcePage;
    }

    // 写入Resource到ResourceES，用于ES嵌套文档的更新
    public ResourceES writeResourceToESHTTP(Resource resource, ResourceES resourceES) {

        String esHost = esNodes.substring(0, esNodes.indexOf(":"));
        String url = "http://" + esHost + ":9200/" + indexName + "/resource/" + resource.getId().toString();
        // set body json
        Map<String, Object> resource_es = new HashMap<>();
        resource_es.put("id", resource.getId());
        resource_es.put("crop_id", resource.getCropId());
        resource_es.put("user_id", resource.getUserId());
        resource_es.put("create_time", resource.getCreateTime());
        resource_es.put("edit_time", resource.getEditTime());
        resource_es.put("title", resource.getTitle());
        resource_es.put("text", resource.getText());
        resource_es.put("superior", resource.getSuperior());
        resource_es.put("recognition", resource.getRecognition());
        resource_es.put("opposition", resource.getOpposition());
        resource_es.put("pageview", resource.getPageview());
        resource_es.put("collection", resource.getCollection());
        resource_es.put("group_id", resource.getGroupId());
        resource_es.put("label_id", resource.getLabelId());
        resource_es.put("permissionId", resource.getPermissionId().toString());

        //进行附件解析
        List<Object> attach_list = new ArrayList<>();
        List<Attachment> attachmentList = attachmentDao.findAllByResourceId(resource.getId());
        if (attachmentList.size() != 0) {
            log.info(this.getClass().getName() + "----in----" + "取资源附件用于嵌套文档更新" + "----");
            //使用Tika进行附件解析
            attachmentList.forEach(attachment -> {
                String text = TikaUtils.parseFile(downloadUrl + attachment.getUrl()); //暂时用这种方法实现一下
                attachment.setOverTime(new Timestamp(new Date().getTime()));
                // set body json
                Map<String, Object> single_attach = new HashMap<>();
                single_attach.put("resourceId", resource.getId());
                single_attach.put("attachmentId", "attach_" + resource.getId() + "_" + attachment.getId());
                single_attach.put("createTime", attachment.getCreateTime());
                single_attach.put("name", attachment.getName());
                single_attach.put("url", attachment.getUrl());
                single_attach.put("attach_text", text);
                if (text.equals("Error parsing file") || text.equals("Error reading file")) {
                    single_attach.put("status", text);
                    attachment.setStatus(Attachment.AttachmentEnum.FAILED);
                } else {
                    single_attach.put("status", "SUCCESS");
                    attachment.setStatus(Attachment.AttachmentEnum.SUCCESS);
                }
                attachmentDao.saveAndFlush(attachment);
                attach_list.add(single_attach);
            });
        }
        resource_es.put("attachment", attach_list);

        //进行评论解析
        List<Object> comment_list = new ArrayList<>();
        List<Comment> commentList = commentDao.findByResourceId(resource.getId());
        commentList.forEach(comment -> {
            Map<String, Object> single_comment = new HashMap<>();
            single_comment.put("resourceId", comment.getResourceId());
            single_comment.put("user_id", comment.getUserId());
            single_comment.put("comment", comment.getContent());
            single_comment.put("time", comment.getTime());
            single_comment.put("userName", comment.getUserName());
            single_comment.put("picName", comment.getPicName());
            comment_list.add(single_comment);
        });
        resource_es.put("comment", comment_list);

        JSONObject json = new JSONObject(resource_es);
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        // http request

        RequestBody body = RequestBody.create(JSON, json.toString());
        Request request = new Request.Builder().url(url).put(body).build();
        OkHttpClient client = new OkHttpClient();
        try {
            Response response = client.newCall(request).execute();
            if (response.code() == 201) {
                log.info(this.getClass().getName() + "----out----" + "新建ES文档成功" + "----");
            } else if (response.code() == 200) {
                log.info(this.getClass().getName() + "----out----" + "更新ES文档成功" + "----");
            } else {
                log.info(this.getClass().getName() + "----out----" + "更新ES文档失败" + "----");
            }
            response.close();
        } catch (IOException e) {
            e.printStackTrace();
            log.info(this.getClass().getName() + "----out----" + "更新ES文档异常" + "----");
        }


        log.info(this.getClass().getName() + "----out----" + "返回写入后的resourceES" + "----");
        return resourceES;
    }

    // 获取时间周期
    public String getPeriod() {
        log.info(this.getClass().getName() + "----in----" + "获取ES更新周期" + "----");
//        String cron = cronDao.findByCronId(1).getCron();
//        int length = cron.length();
//        return cron.substring(2, length).replaceAll("[*]", "").replaceAll("[?]", "").trim();
        //TODO:暂时写死，后续考虑计算方法
        log.info(this.getClass().getName() + "----out----" + "返回180(后端写死)" + "----");
        return "180";
    }


    @Scheduled(cron = "0 0/60 * * * ?")
    public void test1() {
        log.info(this.getClass().getName() + "----out----" + "定时更新附件检索" + "----");
        String updateBody = Attachment.AttachmentUpdate.UPDATE_SCHEDULE_BODY.toString();
        RequestBody requestBody = FormBody.create(MediaType.parse("application/json; charset=utf-8"),updateBody);
        OkHttpClient client2 = new OkHttpClient();
        String esHost = esNodes.substring(0, esNodes.indexOf(":"));
        String url = "http://" + esHost + ":9200/" + indexName + "/_search?size=10000&from=0";
        Request request = new Request.Builder().url(url).post(requestBody).build();
        try {
            String result = client2.newCall(request).execute().body().string();
            JSONObject json1 = JSONObject.parseObject(result);
            Integer att_num = json1.getJSONObject("hits").getJSONArray("hits").size();
            for (int i = 0; i < att_num; i++) {
                String att_id = json1.getJSONObject("hits").getJSONArray("hits").getJSONObject(i).get("_id").toString();
                Resource resource = resourceDao.findResourceById(Integer.parseInt(att_id));
                ResourceES resourceES = new ResourceES();
                writeResourceToESHTTP(resource, resourceES);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
