package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.LabelInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Repository
public interface LabelInfoDao extends JpaRepository<LabelInfo, Integer> {
    Optional<LabelInfo> findById(Integer id);

    LabelInfo findAllById(Integer id);

    List<LabelInfo> findByUplevelId(Integer uplevelId);

    Optional<LabelInfo> findByLabelName(String labelName);
}
