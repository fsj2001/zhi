package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.Template;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Author: ZhaoHaotian.
 * Date: 2021/4/27.
 */
@Repository
public interface TemplateDao extends JpaRepository<Template, Integer> {
    Optional<Template> findById(Integer id);

}
