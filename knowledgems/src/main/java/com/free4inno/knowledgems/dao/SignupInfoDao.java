package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.SignupInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Author HUYUZHU.
 * Date 2021/1/29 16:25.
 */
@Repository
public interface SignupInfoDao extends JpaRepository<SignupInfo, Integer> {
    int countAllByStatus(Integer status);

    Page<SignupInfo> findAllByStatus(Integer status, Pageable pageable);

    Page<SignupInfo> findAllByStatusAndNameLikeOrStatusAndTelnumberLikeOrStatusAndMailLike(
            Integer status1, String name, Integer status2, String telnumber, Integer status3, String mail, Pageable pageable);
}
