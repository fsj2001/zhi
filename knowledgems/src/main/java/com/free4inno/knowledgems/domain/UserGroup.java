package com.free4inno.knowledgems.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.sql.Timestamp;

/**
 * Author HaoYi.
 * Date 2020/9/11.
 */
@Entity
@Table(name = "user_group")
@Data
public class UserGroup {
    @Transient
    private String groupName;
    @Transient
    private String realName;
    @Transient
    private Timestamp createTime;
    @Transient
    private String groupInfo;
    @Transient
    private String leader;
    @Transient
    private int memberNum;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id; //主键

    @Column(name = "crop_id")
    private Integer cropId; //CropId

    @Column(name = "user_id")
    private Integer userId; //用户Id

    @Column(name = "group_id")
    private Integer groupId; //用户组Id

    @Column(name = "permission")
    private Integer permission; //权限，1为群主，2为管理员，0为成员

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getGroupInfo() {
        return groupInfo;
    }

    public void setGroupInfo(String groupInfo) {
        this.groupInfo = groupInfo;
    }

    public String getLeader() {
        return leader;
    }

    public void setLeader(String leader) {
        this.leader = leader;
    }

    public int getMemberNum() {
        return memberNum;
    }

    public void setMemberNum(int memberNum) {
        this.memberNum = memberNum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCropId() {
        return cropId;
    }

    public void setCropId(Integer cropId) {
        this.cropId = cropId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getPermission() {
        return permission;
    }

    public void setPermission(Integer permission) {
        this.permission = permission;
    }

    public UserGroup() {

    }

    public UserGroup(int id) {
        this.id = id;
    }
}
